import {EndPointConfig} from "../utils/endPointConfig";
import {  addContactFailure,
          addContactRequest,
          addContactSuccess,
          getAllContactRequest,
          getAllContactSuccess,
          getAllContactFailure,
          getContactFailure,
          getContactSuccess,
          getContactRequest,
          deleteContactFailure,
          deleteContactRequest,
          deleteContactSuccess
        
        } 
from "../redux/actions/contactTypes";
import axios from "axios";

export const addContactData = (payload)=>{
  return (dispatch) => {
    dispatch(addContactRequest())    
    return axios.post(EndPointConfig.contact.create,payload)
          .then(response => {
            return response.data
          })
          .then(res => {
            alert(res['message'])
            dispatch(addContactSuccess(res['data']))
          })
          .catch(error => {
            dispatch(addContactFailure(error))
          });
  };
}


export const getAllContactData = ()=>{
  return (dispatch) => {
    dispatch(getAllContactRequest())    
    return axios.get(EndPointConfig.contact.getContacts)
          .then(response => {
            return response.data
          })
          .then(res => {
            dispatch(getAllContactSuccess(res['data']))
          })
          .catch(error => {
            dispatch(getAllContactFailure(error))
          });
  };
}


export const getContactData = (email)=>{
  return (dispatch) => {
    dispatch(getContactRequest())    
    return axios.get(EndPointConfig.contact.getContact(email))
          .then(response => {
            return response.data
          })
          .then(res => {
            dispatch(getContactSuccess(res['data']))
          })
          .catch(error => {
            dispatch(getContactFailure(error))
          });
  };
}

export const deleteContactData = (email)=>{
  return (dispatch) => {
    dispatch(deleteContactRequest())    
    return axios.delete(EndPointConfig.contact.delete(email))
          .then(response => {
            return response.data
          })
          .then(res => {
            dispatch(deleteContactSuccess(res['data']))
          })
          .catch(error => {
            dispatch(deleteContactFailure(error))
          });
  };
}