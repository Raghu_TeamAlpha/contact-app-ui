export const isNull = (value)=>{
    switch(typeof value){
        case 'object':
            if(value === null || Object.keys(value).length === 0){
                return true
            }
            if( Object.keys(value).length && !Array.isArray(value)) {
                return false
            }
            if(Array.isArray(value) && value.length){
                return false
            }
            break;
        
        case 'string':
        case 'number':
            if(value.toString().length) {
                return false
            } else {
                return true
            }
        case 'undefined':
            return true
        default:
            return false
    }
    return true;
}

export const validateEmail = (email)=>{

    const emailPattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

    return !emailPattern.test(email);
}

export const validatePhone = (phone)=>{
    const phonePattern = /^\d{10}$/
    return !phonePattern.test(phone);
}