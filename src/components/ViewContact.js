import React, { Component } from "react";
import {ContactCard} from '../containers/ContactCard';
import {getAllContactData} from "../api/contactApi";
import { connect } from 'react-redux';
class ViewContact extends Component{

    constructor(props){
        super(props)
        this.state = {
            contacts:[]
        }
    }
    componentDidMount (){
         this.props.getAllContactData()
       
    }

    componentDidUpdate(prevProps){

        if(this.props.loading !== prevProps.loading
            && !this.props.loading 
            ){

            this.setState({
                contacts:this.props.contacts
            })

        }


        if(this.props.contactDeleted !== 
            prevProps.contactDeleted
            && this.props.contactDeleted ){
                this.props.getAllContactData()
        }
       
       
    }

    render(){

        return(
            <div>
            {
                this.state.contacts.length > 0 ?
                    <ContactCard contacts = {this.state.contacts}/>
                :
                <div> No contacts Available</div>
            }
                
            </div>
        )
    }
}


const mapStateToProps = (state) =>{

     return {
         contacts: state.contacts,
         loading:state.loading,
         contactDeleted:state.contactDeleted
     }
 }
 
 const mapDispatchToProps = (dispatch) =>{
     return {
        getAllContactData: ()=> {return dispatch(getAllContactData())}
     }
 }
 
export default connect(mapStateToProps,mapDispatchToProps)(ViewContact);