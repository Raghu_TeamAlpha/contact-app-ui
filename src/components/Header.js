import React, { Component } from "react";
import "../styles/Header.scss"
export class Header extends Component{

    render(){
        return(
            <div className = "header-container">
                <span className="main-header">Contact App</span>
                <span className="sub-header">A Simple Contact App</span>
            </div>
        )
    }
}