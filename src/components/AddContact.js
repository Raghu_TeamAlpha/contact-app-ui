import React, { Component } from "react";
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import "../styles/AddContact.scss";
import { Card } from "react-bootstrap";
import {FaUserCircle,FaTrash } from 'react-icons/fa';
import {Link} from 'react-router-dom';
import {contactAddressRequired,contactNameRequired,contactProfileRequired,
    emailRequired,emailInvalid,phoneNumberRequired,phoneNumberInvalid
} from "../common/errorMessages";
import {validateEmail,validatePhone,isNull} from './../common/validation';
import {addContactData,getContactData} from '../api/contactApi';
import { connect } from 'react-redux';
class AddContact extends Component{
    
    contactEmailError = null;
    contactPhoneNumberError = null;
    constructor(props){
        super(props)
        this.state = {
            handleFileInput : React.createRef(null),
            file: null,
            contactName:'',
            contactEmail:'',
            contactPhoneNo:'',
            contactAddress:'',
            star:false,
            contactNameError:true,
            contactEmailError:true,
            contactPhoneNoError:true,
            contactAddressError:true,
            contactPhotoError:true,
            valid:false,
            contactFormSubmitted:false

        }
       
        this.handleFileChange = this.handleFileChange.bind(this)
        this.handleUploadFile = this.handleUploadFile.bind(this)
    }
    
    componentDidMount(){
       
  
        if(this.props['match']['params']['email']){
         
            this.props.getContactData(this.props['match']['params']['email'])
        
        }
    }

    componentDidUpdate(prevProps){
       
        if(this.props.loading !== prevProps.loading
            && !this.props.loading 
            ){
                const contact = this.props.contact
            this.setState({
                contactName:contact.name,
                contactEmail:contact.email,
                contactPhoneNo:contact.phone_no,
                contactAddress:contact.address,
                star:Number(contact.star)!=0 ? true:false,
            })

        }
    }

    validation = ()=>{

        return (
            isNull(this.state.contactName) ||
            isNull(this.state.contactEmail) ||
            isNull(this.state.contactPhoneNo) ||
            isNull(this.state.contactAddress) ||
            isNull(this.state.file)
        )

    }

    handleUploadFile(){
       this.state.handleFileInput.current.click()
       
    }
    handleFileChange(event){
        this.setState({
            file: URL.createObjectURL(event.target.files[0])
        })
    }

    removeFile = ()=>{
        this.setState({
            file:null
        })
    }

    handleSubmitContactForm(event){
        event.preventDefault()
        this.setState({
            contactFormSubmitted:true
        })
        if(this.validation()){
            this.setState({
                valid:false
            })
        } else {
            this.setState({
                valid:true
            })

            const payload = {
                name:this.state.contactName,
                email:this.state.contactEmail,
                phone_no:this.state.contactPhoneNo,
                address:this.state.contactAddress,
                star:this.state.star,
                user_image:this.state.file
            }
            this.props.addContactData(payload)

        }

    }

    handleEmailValidation = (value)=>{
   
        if(isNull(value)){
            this.contactEmailError=emailRequired
            return true
            
        } else if (validateEmail(value)){
            this.contactEmailError=emailInvalid
            return true
        } else {
            this.contactEmailError=null
           return false
        }

    }

    handlePhoneNoValidation = (value)=>{
        if(isNull(value)){
            this.contactPhoneNumberError =  phoneNumberRequired   

            return true
            
        } else if (validatePhone(value)){
            this.contactPhoneNumberError =  phoneNumberInvalid  
         
            return true
        } else {
            this.contactPhoneNumberError =  null 
        
            return false
        }
    }



    render(){

       return(
            <div>
            <div className="add-container">
            <Link to = {'../view-contact'}>
                <button className="btn add">View Contacts</button>
            </Link>    
            
            </div>
            <Container  className="form-container">
            <Card style={{padding: "30px"}}>
            <Form onSubmit= {(event)=>{this.handleSubmitContactForm(event)}}>

            <Form.File id="user-img-id" style={{textAlign:"center"}}>
                <Form.File.Input  style = {{display:"none"}}
                ref={this.state.handleFileInput}
                onChange={this.handleFileChange}
                />
                {
                    this.state.file ? 
                    <span>
                    <img src={this.state.file} className="image-preview"
                    onClick = {this.handleUploadFile}
                    />
                    <FaTrash onClick={this.removeFile}/>
                    </span>
                    :
                   
                    <FaUserCircle style={{fontSize:"90px", cursor:"pointer"}}
                    onClick = {this.handleUploadFile}
                    />
                    
                }
                {<Form.Control.Feedback 
                style={{display:"block"}}
                type={ 
                    
                    this.state.contactFormSubmitted && 
                    !this.state.valid && 
                   isNull(this.state.file)
                    ?
                    "invalid":
                    "valid"
                }
                >
                { 
                    this.state.contactFormSubmitted && 
                    !this.state.valid && 
                    isNull(this.state.file)
                    ?
                    contactProfileRequired
                    :null
                }
            </Form.Control.Feedback>
                }
                
               
            </Form.File>



            <Form.Group controlId="formBasicEmail">
              <Form.Label>Name</Form.Label>
              <Form.Control type="text" placeholder="Enter name" 
              value={this.state.contactName}
              onChange={(e)=>{this.setState({contactName:e.target.value})}}
              isValid ={
                    this.state.contactFormSubmitted && 
                    this.state.valid && 
                    !isNull(this.state.contactName)
                }
              isInvalid = {
                    this.state.contactFormSubmitted && 
                    !this.state.valid &&
                    isNull(this.state.contactName)
                }
              />
            
              <Form.Control.Feedback 
                type={
                    
                    this.state.contactFormSubmitted && 
                    !this.state.valid &&
                    isNull(this.state.contactName)?
                    "invalid":
                    "valid"}
                    >
                {   
                    this.state.contactFormSubmitted && 
                    !this.state.valid &&
                    isNull(this.state.contactName)
                    ?
                    contactNameRequired:
                    null}
            </Form.Control.Feedback>

            </Form.Group>

            <Form.Group controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="text" placeholder="Enter email" 
                value={this.state.contactEmail}
                onChange={(e)=>{this.setState({contactEmail:e.target.value})}}
                isValid ={
                    this.state.contactFormSubmitted && 
                    this.state.valid && 
                    !this.handleEmailValidation(this.state.contactEmail)
                }
              isInvalid = {
                    this.state.contactFormSubmitted && 
                    !this.state.valid && 
                    this.handleEmailValidation(this.state.contactEmail)
                }
                
                
                />
                <Form.Control.Feedback 
                type={ 
                    
                    this.state.contactFormSubmitted && 
                    !this.state.valid && 
                    this.handleEmailValidation(this.state.contactEmail)
                    ?
                    "invalid":
                    "valid"
                }
                >
                { 
                    this.state.contactFormSubmitted && 
                    !this.state.valid && 
                    this.handleEmailValidation(this.state.contactEmail)
                    ?
                    this.contactEmailError
                    :null
                }
            </Form.Control.Feedback>
            </Form.Group>

            <Form.Group controlId="formBasicEmail">
                <Form.Label>Phone Number</Form.Label>
                <Form.Control type="number" placeholder="Enter phone number" 
                value={this.state.contactPhoneNo}
                onChange={(e)=>{this.setState({contactPhoneNo:e.target.value})}}
                isValid ={
                    this.state.contactFormSubmitted && 
                    this.state.valid && 
                    !this.handlePhoneNoValidation(this.state.contactPhoneNo)
                }
              isInvalid = {
                    this.state.contactFormSubmitted && 
                    !this.state.valid && 
                    this.handlePhoneNoValidation(this.state.contactPhoneNo)
                }
                
                
                
                />

                <Form.Control.Feedback 
                type={ 
                    
                    this.state.contactFormSubmitted && 
                    !this.state.valid && 
                    this.handlePhoneNoValidation(this.state.contactPhoneNo)
                    ?
                    "invalid":
                    "valid"
                }
                >
                { 
                    this.state.contactFormSubmitted && 
                    !this.state.valid && 
                    this.handlePhoneNoValidation(this.state.contactPhoneNo)
                    ?
                    this.contactPhoneNumberError
                    :null
                }
            </Form.Control.Feedback>
            </Form.Group>

            <Form.Group controlId="exampleForm.ControlTextarea1">
            <Form.Label>Address</Form.Label>
            <Form.Control as="textarea" rows={3}  placeholder="Enter address"
            value={this.state.contactAddress}
            onChange={(e)=>{this.setState({contactAddress:e.target.value})}}
            isValid ={
                this.state.contactFormSubmitted && 
                this.state.valid && 
                !isNull(this.state.contactAddress)
            }
          isInvalid = {
                this.state.contactFormSubmitted && 
                !this.state.valid &&
                isNull(this.state.contactAddress)
            }     
            
            />
            <Form.Control.Feedback 
            type={
                
                this.state.contactFormSubmitted && 
                !this.state.valid &&
                isNull(this.state.contactAddress)?
                "invalid":
                "valid"}
                >
            {   
                this.state.contactFormSubmitted && 
                !this.state.valid &&
                isNull(this.state.contactAddress)
                ?
                contactAddressRequired:
                null}
        </Form.Control.Feedback>
           
          </Form.Group>
            <Form.Group controlId="formBasicCheckbox">
              <Form.Check type="checkbox" label="Mark as star" 
              value={this.state.star}
              onChange={(e)=>{
                this.setState({star:e.target.checked})
                }}
              
                
                
                />
            </Form.Group>
            <Button variant="primary" type="submit">
              Submit
            </Button>
          </Form>
          </Card>
            </Container>
            </div>
        )
    }
}

const mapStateToProps = (state) =>{
    return {
        contacts: state.contacts,
        contact:state.contact,
        loading:state.loading
    }
}

const mapDispatchToProps = (dispatch) =>{
    return {
        addContactData: (payload)=> {return dispatch(addContactData(payload))},
        getContactData:(email) => dispatch(getContactData(email))
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(AddContact);