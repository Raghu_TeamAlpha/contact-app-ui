import {
    ADD_CONTACT_FAILURE,
    ADD_CONTACT_REQUEST,
    ADD_CONTACT_SUCESS,
    GET_ALL_CONTACT_REQUEST,
    GET_ALL_CONTACT_SUCESS,
    GET_ALL_CONTACT_FAILURE,
    GET_CONTACT_FAILURE,
    GET_CONTACT_SUCESS,
    GET_CONTACT_REQUEST,
    DELETE_CONTACT_FAILURE,
    DELETE_CONTACT_SUCESS,
    DELETE_CONTACT_REQUEST } 
from '../../actions/types'

export const addContactRequest = ()=>{
    return  {
                type:ADD_CONTACT_REQUEST,
                isLoading:true
            }
}

export const addContactSuccess = (contact) =>{
    return {
        type:ADD_CONTACT_SUCESS,
        isLoading:false,
        payload:contact
    }
}

export const addContactFailure = (error) =>{
    return {
        type:ADD_CONTACT_FAILURE,
        isLoading:false,
        error:error
    }
}


export const getAllContactRequest = ()=>{
    return  {
                type:GET_ALL_CONTACT_REQUEST,
                isLoading:true
            }
}

export const getAllContactSuccess = (contact) =>{
    return {
        type:GET_ALL_CONTACT_SUCESS,
        isLoading:false,
        payload:contact
    }
}

export const getAllContactFailure = (error) =>{
    return {
        type:GET_ALL_CONTACT_FAILURE,
        isLoading:false,
        error:error
    }
}


export const getContactRequest = ()=>{
    return {
                type:GET_CONTACT_REQUEST,
                isLoading:true
            }
}

export const getContactSuccess = (contact) =>{
    return {
        type:GET_CONTACT_SUCESS,
        isLoading:false,
        payload:contact
    }
}

export const getContactFailure = (error) =>{
    return {
        type:GET_CONTACT_FAILURE,
        isLoading:false,
        error:error
    }
}

export const deleteContactRequest = ()=>{
    return {
                type:DELETE_CONTACT_REQUEST,
                isLoading:true
            }
}

export const deleteContactSuccess = (contact) =>{
    return {
        type:DELETE_CONTACT_SUCESS,
        isLoading:false,
        payload:contact
    }
}

export const deleteContactFailure = (error) =>{
    return {
        type:DELETE_CONTACT_FAILURE,
        isLoading:false,
        error:error
    }
}