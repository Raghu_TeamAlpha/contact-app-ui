import {
    ADD_CONTACT_FAILURE,
    ADD_CONTACT_REQUEST,
    ADD_CONTACT_SUCESS,
    GET_ALL_CONTACT_REQUEST,
    GET_ALL_CONTACT_SUCESS,
    GET_ALL_CONTACT_FAILURE,
    GET_CONTACT_FAILURE,
    GET_CONTACT_SUCESS,
    GET_CONTACT_REQUEST,
    DELETE_CONTACT_FAILURE,
    DELETE_CONTACT_REQUEST,
    DELETE_CONTACT_SUCESS 
} 
from '../../actions/types'

const initialState = {
    loading:false,
    contacts:[],
    error:'',
    contact:null,
    contactDeleted:false
}

export const contactReducer = (state = initialState,action)=>{
    switch(action.type){
      
        case ADD_CONTACT_REQUEST: return {
            ...state,
            loading: true
        }
        case ADD_CONTACT_SUCESS: return {
            ...state,
            loading:false,
            error:''
        }
        case ADD_CONTACT_FAILURE: return {
            ...state,
            loading:false,
            error:action.error
        }

        case GET_ALL_CONTACT_REQUEST:return {
            ...state,
            loading:true,
            error:''
        }
        case GET_ALL_CONTACT_SUCESS:return {
            ...state,
            loading:false,
            contacts:action.payload,
            error:''
        }
        case GET_ALL_CONTACT_FAILURE:return {
            ...state,
            loading:false,
            contacts:action.error
        }

        case GET_CONTACT_REQUEST:return {
            ...state,
            loading:true,
            error:'',
            contact:null
        }
        case GET_CONTACT_SUCESS:
        return {
            ...state,
            loading:false,
            contact:action.payload,
            error:''
        }
        case GET_CONTACT_FAILURE:return {
            ...state,
            loading:false,
            contact:null,
            error:action.error
        }

        case DELETE_CONTACT_REQUEST: return {
            ...state,
            loading: true,
            contactDeleted:false
        }
        case DELETE_CONTACT_SUCESS: return {
            ...state,
            loading:false,
            error:'',
            contactDeleted:true
        }
        case DELETE_CONTACT_FAILURE: return {
            ...state,
            loading:false,
            error:action.error,
            contactDeleted:false
        }
        default: return state


    }
}