import {isProd} from './env'


//const baseUrl = isProd? "http://contact-app-mysql.herokuapp.com" : "http://localhost:4000"
const baseUrl =process.env.REACT_APP_API_URL
export const EndPointConfig = {

    contact :{
        create:`${baseUrl}/api/user/add`,
        getContacts:`${baseUrl}/api/user/all`,
        getContact:(email)=>{return `${baseUrl}/api/user/${email}`},
        update: `${baseUrl}/api/user/update`,
        delete:(email)=>{ return `${baseUrl}/api/user/${email}`}

    }

}