import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Route,BrowserRouter as Router, Switch} from 'react-router-dom';
import AddContact from "./components/AddContact";
import {Footer} from "./components/Footer";
import {Header} from "./components/Header";
import {ListContact} from "./components/ListContact";
import ViewContact from "./components/ViewContact";
import {Provider} from 'react-redux';
import {store} from "./redux/store"
function App() {
  return (
    <div>
    {process.env.REACT_APP_API_URL}
    <Provider store={store}>
    <Router>
    <Header/>
      <Switch>
        <Route path="/add-contact"  component ={AddContact}   />
        <Route path="/edit-contact/:email"  component ={AddContact}   />
        <Route path="/list-contact"  component ={ListContact}   />
        <Route path="/view-contact"  component ={ViewContact}   /> 
        <Route path="/"  component ={ViewContact}   />
      </Switch>
    <Footer/>
    </Router>
    </Provider>
    </div>
  );
}

export default App;
