import React,{Component} from "react";
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import "../styles/DeleteModal.scss"
import {deleteContactData} from "../api/contactApi";
import { connect } from 'react-redux';
 class DeleteModal extends Component {

    constructor(props){
        super(props)
        this.state = {
            contact:null,
            name:''
        }
    }

    componentDidMount(){
        
    }

    componentDidUpdate(){
        console.log(this.props)
        if(!this.state.contact ){
            this.setState( {
                contact:this.props.contact
            }
            )
        }
    }

    deleteContact(){

        if(this.state.contact.name === this.state.name){
            console.log("delete request ")

            this.props.deleteContactData(this.state.contact.email)
            this.props.closeModal()
        }else{
            alert("enterd username is incorrect ")
        }
    }

    render(){

        return(
            
            this.state.contact ?

            <Modal show={this.props.open} onHide={this.props.closeModal} backdrop="static">
                <Modal.Header closeButton>
                    
                    <Modal.Title>Are you sure you want to delete the 
                    <span style={{color:"red",padding:"0px 5px"}}>
                    {`" ${this.state.contact['name'] }"`}
                    </span>
                    user?</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>User Name</Form.Label>
                            <Form.Control type="text" placeholder="Enter Enter " 
                            onChange = {(e)=>{this.setState({name:e.target.value})}}
                            />
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={this.props.closeModal}>Close</Button>
                    <Button variant="primary" onClick = {()=>{this.deleteContact()}}>Delete</Button>
                </Modal.Footer>
            </Modal>

            :
            null
            
        )
    }
}


const mapStateToProps = (state) =>{

    return {
        contacts: state.contacts,
        loading:state.loading
    }
}

const mapDispatchToProps = (dispatch) =>{
    return {
        deleteContactData: (email)=> {return dispatch(deleteContactData(email))}
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(DeleteModal);