import React, { Component } from "react";
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Card from 'react-bootstrap/Card'
import { FaRegStar,FaUserCircle,FaStar } from 'react-icons/fa';
import "../styles/ContactCard.scss"
import DeleteModal from "./DeleteModal";
import {Link} from 'react-router-dom';
export class ContactCard extends Component{

    
    constructor(props){
        super(props)
        this.state = {
            openDeleteModal: false,
            contact:null
        }
    }

    openDeleteModal = (contact)=>{
        this.setState({
            openDeleteModal:true,
            contact:contact
        })


    }
    closeModal = ()=>{
        this.setState({
            openDeleteModal:false
        }) 
    
        
    }
    render(){
        const contacts = this.props.contacts
        return(
            <div>
                <Container style={{minHeight:"calc(100vh - 150px)"}}>
                    <div className="add-container">
                    <Link to = {'add-contact'}>
                        <button className="btn add">Add</button>
                    </Link>    
                    
                    </div>
            {contacts.map((contact) => 
              
                
                <Card className="contact-card" key={contact['email']}>
                <Row className="mt-b-15">
                    <Col md = {1} className = "line-center">
                        <span className ="star"><FaRegStar /></span>                   
                    </Col>
                    <Col md = {2} className = "line-center">
                        {
                         
                            contact['user_image'] ?
                            <span className ="user-profile">
                                <FaUserCircle/>
                            </span>:
                            <span  className ="user-profile">
                                <img src={contact['user_image']}/>
                            </span>    
                            
                        }
                       
                    </Col>
                    <Col md = {5} className="user-info">
                        <div className="text-color">{contact['name']}</div>
                        <div>{contact['phone_no']}</div>
                        <div>{contact['email']}</div>
                        <div>{contact['address']}</div>
                    </Col>
                    <Col md = {2} className = "line-center">
                       <button type="button" className="btn delete"
                       onClick = {()=>{this.openDeleteModal(contact)}}
                       >Delete</button>
                    </Col>
                    <Col md = {2} className = "line-center">
                    <Link to = {`edit-contact/${contact['email']}`}>
                    <button type="button" className="btn edit">Edit</button>
                    </Link>
                    </Col>
                </Row>
            </Card>
                )}
        </Container>
              
            <DeleteModal open={this.state.openDeleteModal} closeModal = {this.closeModal}
            contact = {this.state.contact}
            />
            </div>
        )
    }
}